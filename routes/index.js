
/*
 * GET home page.
 */

exports.index = function(req, res){
    res.render('index', { title: 'Feed me links' });
};

/*
 * POST link feeding AJAX page.
 */
//TODO: reintroduce DI for models
exports.giveLink = function($url, $Link, $error, $Page){
    return function(req, res) {
        var requestURL = req.body.link;

        var checkURL = function ( url ) {
            return $error.errorCheckAndResponse(
                !$url.check( requestURL ),
                { 
                    error: { 'message': 'Invalid protocol in url, please use HTTP or HTTPS only' } 
                }
            );
        };

        var saveLink = function( url, page, callback ) {
            var givenLink = new $Link.model( { url: normalizedURL } );
            
            givenLink.save( function(err) { 
                $error.errorCheckAndResponse(
                    err,
                    { 
                        error: { 'message': 'Error saving link' }
                    },
                    callback
                );
            });
        };

        var sendLink = function() {
            $Link.model.random( function( err, link ) {

                $error.errorCheckAndResponse( 
                    err || !link,
                    {
                        error: { 'message': 'Database error' }
                    },
                    function() {
                        var result = link.toObject();
                        res.json( { title: result.title, url: result.url } );
                    });
                //TODO: test this
    /*
                if (err || !link){
                    console.log(err);
                    res.json( 500, {
                        error: { 'message': 'Database error' }
                    });
                } else {
                    var result = link.toObject();
                    res.json( { title: result.title, url: result.url } );
                }
    */
            });
        };

        if ( checkURL( requestURL ) ) {
            var page = new $Page( requestURL );
            if ( page.isValid() ) {

                var normalizedURL = $url.normalize( requestURL );
                
                saveLink( 
                    normalizedURL, 
                    page,
                    sendLink
                );
            } else {
               $error.errorCheckAndResponse(true, 'message');
            }
        } 
    };
};
