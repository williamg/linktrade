var assert = require('assert');
var models = require('../../models');

module.exports.run = function() {
    describe( 'URL', function() {
        describe( 'type', function() {
            it( 'should not be undefined' , function() {
                assert.throws( function() { models.url.normalize( undefined ) } );
            });
            it( 'should not be undefined' , function() {
                assert.throws( function() { models.url.check( undefined ) } );
            });
        });
        describe('protocol', function() {
            it( 'should not be accepted if it is not http/https', function() {
                assert( !models.url.check( "ftp://example.com/" ) );
                assert( !models.url.check( "steam://example.com/" ) );
                assert( models.url.check( "http://example.com/" ) );
                assert( models.url.check( "https://example.com/" ) );   
            });
        });
        describe( 'protocol', function() {
            it( 'should not matter between http and https', function() {
                assert.strictEqual(
                    models.url.normalize( "http://example.com/" ), 
                    models.url.normalize( "https://example.com" )
                );   
            });
            it( 'should not matter if it is http/https', function() {
                assert.strictEqual(
                    models.url.normalize( "http://example.com/" ), 
                    models.url.normalize( "example.com" )
                );   
                assert.strictEqual(
                    models.url.normalize( "https://example.com/" ), 
                    models.url.normalize( "example.com" )
                );  
            });
        });
        describe( 'slashes', function() {
            it( 'should not matter which direction they are', function() {
                assert.strictEqual(
                    models.url.normalize( "http://example.com/" ), 
                    models.url.normalize( "http:\\\\example.com\\" )
                );
                assert.strictEqual(
                    models.url.normalize( "http://example.com/" ), 
                    models.url.normalize( "http://example.com\\" )
                );
            });
        });
        describe( 'case', function() {
            it( 'should not matter', function() {
                assert.strictEqual(
                    models.url.normalize( "http://EXAMPLE.COM/" ), 
                    models.url.normalize( "http://example.com/" )
                );
                assert.strictEqual(
                    models.url.normalize( "http://EXAMPLE.COM/" ), 
                    models.url.normalize( "http://example.COM/" )
                );
                assert.strictEqual(
                    models.url.normalize( "http://EXAMPLE.COM/" ), 
                    models.url.normalize( "HTTP://example.COM/" )
                );
            });
        });
        describe( 'encoding', function() {
            it( 'should not matter', function() {
                assert.strictEqual(
                    models.url.normalize( "http://example.com/%24" ), 
                    models.url.normalize( "http://example.com/$" )
                );
                assert.strictEqual(
                    models.url.normalize( "http://example.com/%40" ), 
                    models.url.normalize( "http://example.com/@" )
                );
                assert.strictEqual(
                    models.url.normalize( "http://EXAMPLE.COM/%20" ), 
                    models.url.normalize( "http://example.COM/ " )
                );
            });
        });
        describe( 'whitespace', function() {
            it( 'should not matter', function() {
                assert.strictEqual(
                    models.url.normalize( "http://example.com/ " ), 
                    models.url.normalize( "http://example.com/" )
                );
                assert.strictEqual(
                    models.url.normalize( "      http://example.com/" ), 
                    models.url.normalize( "http://example.com/" )
                );
            });
        });
        describe( 'default port', function() {
            it( 'should not matter', function() {
                assert.strictEqual(
                    models.url.normalize( "http://example.com:80/ " ), 
                    models.url.normalize( "http://example.com/" )
                );
                assert.notStrictEqual(
                    models.url.normalize( "http://example.com:8080" ), 
                    models.url.normalize( "http://example.com/" )
                );
            });
        });
        describe( 'dot segments', function() {
            it( 'should not matter', function() {
                //on some web servers this may not be equivalent since the '..' or '.' will actually affect the www path
                //but we are assuming that it will not make a practical difference in links given, so this test is still valid
                assert.strictEqual(
                    models.url.normalize( "http://example.com/../a/b/../c/d.html " ), 
                    models.url.normalize( "http://example.com/a/b/c/d.html" )
                );
                assert.strictEqual(
                    models.url.normalize( "http://example.com/.././././../././.././.././../." ), 
                    models.url.normalize( "http://example.com/" )
                );
            });
        });
        describe( 'hash fragment', function() {
            it( 'should not matter', function() {
                assert.strictEqual(
                    models.url.normalize( "http://example.com/page.html#content" ), 
                    models.url.normalize( "http://example.com/page.html" )
                );
                assert.strictEqual(
                    models.url.normalize( "http://example.com/page.html#content=some-stuff=who-knows" ), 
                    models.url.normalize( "http://example.com/page.html" )
                );
            });
        });
        describe( 'query strings', function() {
            it( 'should not matter', function() {
                assert.strictEqual(
                    models.url.normalize( "http://example.com/page.html?username=bad&password=security" ), 
                    models.url.normalize( "http://example.com/page.html" )
                );
            });
        });
        describe( 'www subdomains', function() {
            it( 'should not matter', function() {
                assert.strictEqual(
                    models.url.normalize( "http://www.example.com/" ), 
                    models.url.normalize( "http://example.com/" )
                );
                assert.notStrictEqual(
                    models.url.normalize( "http://ww.example.com/" ), 
                    models.url.normalize( "http://example.com/" )
                );
                assert.notStrictEqual(
                    models.url.normalize( "http://wwww.example.com/" ), 
                    models.url.normalize( "http://example.com/" )
                );
            });
        });
        describe( 'extra slashes', function() {
            it( 'should not matter', function() {
                assert.strictEqual(
                    models.url.normalize( "http://example.com//" ), 
                    models.url.normalize( "http://example.com/" )
                );
                assert.strictEqual(
                    models.url.normalize( "http://example.com///////////" ), 
                    models.url.normalize( "http://example.com/" )
                );
                assert.strictEqual(
                    models.url.normalize( "http://example.com/" ), 
                    models.url.normalize( "http://example.com" )
                );
            });
        });
        describe( 'html versus htm', function() {
            it( 'should not practically matter', function() {
                assert.strictEqual(
                    models.url.normalize( "http://example.com/page.html" ), 
                    models.url.normalize( "http://example.com/page.htm" )
                );
            });
        });
    });
};

