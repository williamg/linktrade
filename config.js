module.exports.projectName = 'linktrade';

module.exports.server = {
    port: (process.env.PORT || 3000)
};

module.exports.mongoUri = process.env.MONGOLAB_URI ||
  process.env.MONGOHQ_URL ||
  'mongodb://localhost/' + exports.projectName;