var linksApp = angular.module("linksApp", []);

linksApp.controller("linkFeeding", function( $scope, $http ) {
    
    $scope.giveLink = function() {
        $scope.linkResultError = null;
        $http.post("/feeding", {"link": $scope.linkGiven}).
            success( function( data, status, headers, config ) {
                $scope.linkResult = data;
            }).
            error( function( data, status, headers, config ) {
                console.log("--giveLink $http error--")
                console.log(data);
                console.log(status);
                console.log(headers);
                console.log(config);
                $scope.linkResultError = data;
                $scope.linkResultError.status = status;
            });

        $scope.linkGiven = ""
    }
});