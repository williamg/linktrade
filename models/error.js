exports.errorCheck = function( err, failback, callback ) {
    if ( err ) {
        failback();
        return false;
    } else {
        callback();
        return true;
    }
};

exports.successCheck = function( err, failback, callback ) {
    errorCheck( !err, failback, callback );
};

//TODO: everything follows the standard error pattern, roll that in to a separate function
exports.errorCheckAndResponse = function( err, json, callback ) {
    errorCheck( 
        err,
        res.json( 500, json ),
        callback
     );
};
/*
exports.errorCheckAndResponse = function( err, json, callback ) {
    if ( err ) {
        res.json( 500, json );
        return false;
    } else {
        callback();
        return true;
    }
};
*/

exports.jsonErrorTemplate = function( message ) {
    return  {
        error: { 
            'message': (message || 'No message provided')
        } 
    };
};