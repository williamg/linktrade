var Mongoose = require('mongoose'); 
var config = require('../config.js');

var db = Mongoose.createConnection( config.mongoUri );

module.exports = {
        Link: require('./Link.js')(db) ,
        url: require('./URL.js'),
        error: require('./error.js')
};