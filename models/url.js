exports.check = function( url ) {
    if ( !url ){
        throw new Error( "URL was invalid type: " + typeof url );
    }

    var normalized = decodeURIComponent(url).toLowerCase();
    //make sure if there's a supplied protocol it is http (no ftp:// and the likes!)
    if ( normalized.match( /:\/\// ) && !normalized.match( /^https?:\/\// ) ){
        return false;
    }

    return true;
};

exports.normalize = function( url ) {
    if ( !url ){
        throw new Error( "URL was invalid type: " + typeof url );
    }
    //note that some of the normalized actually changes the semantics of the URL
    //this is just supposed to group URLs that are "most likely" similar together, not to be completely accurate

    var normalized = decodeURIComponent(url).toLowerCase();
    normalized = normalized.replace( /\\/g, "/" );                  //force one direction of slashes
    normalized = normalized.replace( / /g, "" );                    //get rid of any whitespace

    normalized = normalized.replace( /^https?:\/\// , "" );         //remove protocol portion
    normalized = normalized.replace( /:80\/|:80$/ , "" );           //remove default port
    normalized = normalized.replace( /(\/[./]*\/)(\.*$)?/g , "/");  //remove dot segments in paths (as in 'http://www.example.com/../a/b/../c/./d.html')
    normalized = normalized.replace( /#.+$/ , "" );                 //remove fragment on end of url
    normalized = normalized.replace( /\/\//g , "/" );               //removing extra slashes (this would match the https:// part but we're removing that before this step)
    normalized = normalized.replace( /\?.+/ , "" );                 //removing query string
    normalized = normalized.replace( /^www\./ , "" );               //removing the www. from the beginning of the url
    normalized = normalized.replace( /\/+$/ , "" );                 //remove trailing slashes (not restricted to one)

    //turning .html in to .htm
    if ( normalized.match( /\.html$/ ) ){
        normalized = normalized.substring( 0, normalized.length - 1 ); 
    }

    return normalized;
};