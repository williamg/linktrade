var Mongoose = require('mongoose');

module.exports = function( db ) {

    var schema = new Mongoose.Schema({
            url : { type : String, required : true },
            random_index: { type: [Number], index: '2d', default: ( [Math.random(), 0] ) }
        });

    schema.statics.random = function ( callback ) {
       this.model('links').findOne( 
            { random_index : { $near : [Math.random(), 0] } },
            callback
        );
    };

    var model = db.model('links', schema);

    return {
        schema: schema,
        model: model 
    };
};