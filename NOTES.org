--- notes: ---
* Planned Tasks [0/2]
  - [ ] Scrape websites [1/4]
    - [X] store dependencies
      - [[https://github.com/mikeal/request]]
      - [[https://github.com/MatthewMueller/cheerio]]
    - [ ] MAYBE: do scrape as an object
      - [ ] def. need promises [[https://github.com/kriskowal/q]]
      - [ ] learn constructors
      - [ ] constructor
      - [ ] retrievers (title, content, etc.)
    - [ ] request page
    - [ ] validate page content
    - [ ] take page content (title, maybe others)
    - [ ] store page
  - [ ] Users
    - [ ] take username, generate person key
    - [ ] store info, validate
    - [ ] favourites and such


--- old notes ---
TODO:
- fix angularjs references to http to use service instead (http://nathanleclaire.com/blog/2014/01/04/5-smooth-angularjs-application-tips/)

ITEMS OF INTEREST/THOUGHTS:
- to help unit test:
https://github.com/visionmedia/supertest

- need to add in titles
-- check cache first
-- otherwise request that page


db.docs.findOne( { random_point : { $near : [Math.random(), 0] } } )


xxxxxxxxxxxxxxxxxxxxxxxx
xxxxxxxxxxxxxxxxxx
xxxxxxxxxxxx
xxxxxxxxxxx
xxxxxxxxxx
xxxxxxxxxx
xxxxxxxxxx
xxxxxx
xxxxx
xxxxx
xxxx
x
x
x
x
x
x
x
x



sort by rating


sample weighted towards lower number between 0, count

beta = 4 * (rand - 0.5)^3 + 0.5